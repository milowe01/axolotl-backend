# Form Builder Backend

Handles sending emails and creating JIRA issues from form builder submissions

## Develop

```bash
  $ git clone https://gitlab.cwp.govt.nz/forms/moeforms-demo-backend.git
  $ cd moeforms-demo-backend
  $ git checkout develop
  $ npm run start-dev
```
