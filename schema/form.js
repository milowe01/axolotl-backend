const mongoose = require('mongoose')

let formSchema = mongoose.Schema({
    title: String,
    subtitle: String,
    instructions: String,
    author: String,
    sections: String,
    issueId: String,
    issueUrl: String,
}, {
    timestamps: {
        createdAt: 'created_at',
        modifiedAt: 'modified_at'
    }
})

formSchema.virtual('id').get(function(){
    return this._id.toHexString()
});

formSchema.set('toJSON', { virtuals: true});

module.exports = mongoose.model('Form', formSchema)
