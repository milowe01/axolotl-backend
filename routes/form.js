const express = require('express')
const router = express.Router()
const request = require('superagent')

const mongoose = require('mongoose')
mongoose.Promise = Promise

const clone = require('clone')

const { JIRA_API_ENDPOINT } = require('../config')

/** Schema **/
const Form = require('../schema/form')

/** DB **/
const {
    getForm,
    getAllForms,
    updateForm,
    deleteForm,
    submitForm,
} = require('../db/form')

/** LIB **/
const jiraifyForm = require('../lib/jiraifyForm')


/** GET all forms **/
router.get('/', (req, res) => {
    Form.find({})
        .then(forms => res.json({"forms": forms}))
        .catch(error => {
            res.status(501)
            res.json(error)
        })
})

/** GET all forms with JIRA status' **/
router.get('/status', (req, res) => {
    Form.find({})
        .then(forms => {
            let formsWithIssue = forms.filter(item => item.issueId)
            const issuePromises = formsWithIssue.map(item => {
                return request
                    .get(`${JIRA_API_ENDPOINT}/issue/${item.issueId}?fields=status`)
                    .auth(process.env.JIRA_USERNAME, process.env.JIRA_PASSWORD)
            })
            return Promise.all(issuePromises).then(issueData => ({issueData, forms}))
        })
        .then(({issueData, forms}) => {
            if(issueData) {
                forms = forms.map(form => {
                    let newForm = form.toObject()
                    newForm.id = newForm._id
                    if(newForm.issueId) {
                        newForm.issueStatus = issueData.find(issue => {
                            return issue.body.id === form.issueId
                        }).body.fields.status.name
                    }
                    return newForm
                })
            }
            return forms
        })
        .then(forms => res.json({forms: [forms]}))
        .catch(error => {
            res.status(501)
            res.json(error)
        })
})

/** GET form by ID **/
router.get('/:id', (req, res) => {
    Form.find({_id: req.params.id})
        .then(form => res.json({forms: form}))
        .catch(error => {
            res.status(404)
            res.json({forms: []})
        })
})

/** REMOVE jira id and url from FORM **/
router.get('/:id/resetIssue', (req, res) => {
    Form.findById(req.params.id)
        .then(form => {
            form.issueId = null
            form.issueUrl = null
            return form.save()
        })
        .then(form => res.json(form))
        .catch(error => res.json(error))
})

/** POST a new form to be saved in mongo **/
router.post('/', (req, res) => {
    const data = JSON.parse(Object.keys(req.body)[0])
    const { title, subtitle, author, instructions, sections } = data
    const newForm = new Form({
        title,
        subtitle,
        author,
        instructions,
        sections,
        issueId: null,
        issueUrl: null,
    })
    newForm.save()
        .then(form => res.json({"form": form}))
        .catch(error => res.json(error))
})

/** PATCH/UPDATE a form by Id **/
router.post('/:id/update', (req, res) => {
    const newForm = JSON.parse(Object.keys(req.body)[0])
    updateForm(req.params.id, newForm)
        .then(savedForm => res.json(savedForm))
        .catch(error => res.json(error))
})

/** POST to sumit a form as a new JIRA issue **/
router.post('/:id/submit', (req, res) => {
    Form.findById(req.params.id)
        .then(form =>{
            return request
                .post(`${JIRA_API_ENDPOINT}/issue`)
                .auth(process.env.JIRA_USERNAME, process.env.JIRA_PASSWORD)
                .send(jiraifyForm(form))
        })
        .then(jiraData => {
            Form.findById(req.params.id)
                .then(form => {
                    form.issueId = jiraData.body.id
                    form.issueUrl = jiraData.body.self
                    return form.save()
                        .then(form => {
                            res.json(form)
                        })
                })
        })
        .catch(error => res.json(error))
})

/** DELETE form by ID **/
router.delete('/:id', (req, res) => {
    deleteForm(req.params.id)
        .then(form => res.json(form))
        .catch(error => res.json(error))
})

/** GET jira issue by ID **/
router.get('/jira/:id', (req, res) => {
  request
    .get(`${JIRA_API_ENDPOINT}/issue/${req.params.id}`)
    .auth(process.env.JIRA_USERNAME, process.env.JIRA_PASSWORD)
    .then(response => res.json(JSON.parse(response.text)))
    .catch(error => res.json(error))
});

/** GET form by ID that is parsed for easier human reading **/
router.get('/:id/pretty', (req, res) => {
    Form.find({_id: req.params.id})
        .then(form => res.json({
            form: {
                title: form[0].title,
                subtitle: form[0].subtitle,
                instructions: form[0].instructions,
                sections: JSON.parse(form[0].sections)
            }
        }))
        .catch(error => res.json(error))
})

module.exports = router
