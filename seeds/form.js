[
    {
        "issueUrl": null,
        "issueId": null,
        "sections": "[{\"title\":\"Environment Variables\",\"fields\":[{\"id\":\"0e9039d0-a6f9-11e7-aa1d-f338320b5cd6\",\"type\":{\"key\":\"Basic Text\",\"value\":\"text\"},\"label\":\"What is the IP address for the prod mongoDB?\",\"toolTipContent\":\"\",\"characterLimit\":null,\"hasCharacterLimit\":false,\"options\":[],\"required\":true,\"hasEthnicity\":false,\"hasLanguages\":false,\"hasEmail\":false,\"hasMobile\":false,\"hasHomePhone\":false,\"otherComments\":\"\",\"isUnconfirmed\":false,\"maskType\":null},{\"id\":\"1a737050-a6f9-11e7-aa1d-f338320b5cd6\",\"type\":{\"key\":\"Basic Text\",\"value\":\"text\"},\"label\":\"What is the IP address for the DEV mongoDB?\",\"toolTipContent\":\"\",\"characterLimit\":null,\"hasCharacterLimit\":false,\"options\":[],\"required\":true,\"hasEthnicity\":false,\"hasLanguages\":false,\"hasEmail\":false,\"hasMobile\":false,\"hasHomePhone\":false,\"otherComments\":\"\",\"isUnconfirmed\":false,\"maskType\":null},{\"id\":\"2b8ce150-a6f9-11e7-aa1d-f338320b5cd6\",\"type\":{\"key\":\"File Upload\",\"value\":\"fileUpload\"},\"label\":\"Please upload your project\",\"toolTipContent\":\"\",\"characterLimit\":null,\"hasCharacterLimit\":false,\"options\":[],\"required\":true,\"hasEthnicity\":false,\"hasLanguages\":false,\"hasEmail\":false,\"hasMobile\":false,\"hasHomePhone\":false,\"otherComments\":null,\"isUnconfirmed\":false,\"maskType\":null}],\"isNew\":false}]",
        "instructions": "<ul>\n<li>This form should be saved to the mongodb docker container on the lowe-down DEV-server</li>\n<li>It should not submit it's forms to JIRA, rather emits a mock response from JIRA</li>\n</ul>",
        "subtitle": "It is not Production!",
        "title": "This is the Test Environment",
    },
    {
        "issueUrl": null,
        "issueId": null,
        "sections": "[{\"title\":\"Why do you sometimes not work\",\"fields\":[{\"id\":\"2a10d330-a6fa-11e7-bf8b-11faf913ef25\",\"type\":{\"key\":\"Date Chooser\",\"value\":\"dateChooser\"},\"label\":\"What date were you born?\",\"toolTipContent\":null,\"characterLimit\":null,\"hasCharacterLimit\":false,\"options\":[],\"required\":true,\"hasEthnicity\":false,\"hasLanguages\":false,\"hasEmail\":false,\"hasMobile\":false,\"hasHomePhone\":false,\"otherComments\":null,\"isUnconfirmed\":false,\"maskType\":null},{\"id\":\"42892b10-a6fa-11e7-bf8b-11faf913ef25\",\"type\":{\"key\":\"Checkboxes\",\"value\":\"checkbox\"},\"label\":\"Which of the following apply?\",\"toolTipContent\":null,\"characterLimit\":null,\"hasCharacterLimit\":false,\"options\":[{\"key\":\"I am tall\",\"value\":\"I am tall\"},{\"key\":\"I am not that tall\",\"value\":\"I am not that tall\"},{\"key\":\"I am pretty short\",\"value\":\"I am pretty short\"}],\"required\":true,\"hasEthnicity\":false,\"hasLanguages\":false,\"hasEmail\":false,\"hasMobile\":false,\"hasHomePhone\":false,\"otherComments\":null,\"isUnconfirmed\":false,\"maskType\":null}],\"isNew\":false}]",
        "instructions": "<p>What's up with that?</p>",
        "subtitle": "What's For Lunch Today?",
        "title": "Lunch Time",
    }
]
