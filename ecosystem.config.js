module.exports = {

    apps: [
        {
            name: 'odin-hook',
            script: '/odin-hook/bin/www',
            watch: true,
            node_args: '--harmony',
            merge_logs: true,
            instances: 2,
            exec_mode: 'cluster',
            cwd: '~/odin-hook/',
            env: {
                COMMON_VARIABLE: 'true',
            },
            env_production: {
                NODE_ENV: 'production',
            },
        },
    ],
};
