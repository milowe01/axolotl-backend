/** Schema **/
const Form = require('../schema/form')


/**
 * Get a form by an ID
 * @param id
 * @returns {Promise}
 */
const getForm = (id) => {
    return new Promise((resolve, reject) => {
        Form.findById(id, (error, form) => {
            if (error) return reject(error)
            resolve ({form: [form]})
        })
    })
}

/**
 * Get all forms
 * @returns {Promise}
 */
const getAllForms = () => {
    return new Promise((resolve, reject) => {
        Form.find({}, (error, form) => {
            if (error) reject(error)
            resolve({"forms": form})
        })
    })
}

/**
 * Updates a form's data
 * @param id
 * @param updatedForm
 * @returns {Promise}
 */
const updateForm = (id, updatedForm) => {
    return new Promise((resolve, reject) => {
        Form.findById(id, (error, form) => {
            if (error) return reject(reject)
            form.title = updatedForm.title
            form.subtitle = updatedForm.subtitle
            form.instructions = updatedForm.instructions
            form.author = updatedForm.author
            form.sections = updatedForm.sections
            form.save(() => resolve(form))
        })
    })
}

/**
 * Delete a form by ID
 * @param id
 * @returns {Promise}
 */
const deleteForm = (id) => {
    return new Promise((resolve, reject) => {
        Form.findOneAndRemove({_id: id}, (error, form) => {
            if (error) reject(error)
            resolve({form})
        })
    })
}

/**
 * Append JIRA issue information to a form by ID
 * @param id
 * @param jiraData
 * @returns {Promise}
 */
const submitForm = (id, jiraData) => {
    return new Promise((resolve, reject) => {
        Form.findById(id, (error, form) => {
            if (error) return reject(error)
            form.issueId = jiraData.id
            form.issueUrl = jiraData.self
            form.save(() => resolve(form))
        })
    })
}


/**
 * Remove JIRA issue data from a form by ID
 * @param id
 * @returns {Promise}
 */
const resetFormIssue = (id) => {
    return new Promise((resolve, reject) => {
        Form.findById(id, (error, form) => {
            if (error) return reject(reject)
            form.issueId = null
            form.issueUrl = null
            form.save(() => resolve(form))
        })
    })
}

module.exports = {
    getForm,
    getAllForms,
    updateForm,
    deleteForm,
    submitForm,
    resetFormIssue
}
