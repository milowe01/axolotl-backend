const request = require('supertest')
const test = require('tape')
const app = require('../app')

const mongoose = require('mongoose')

test('that a GET from /forms returns data in the correct format', t => {
    request(app)
        .get('/form')
        .then(response => {
            t.ok(response.body.hasOwnProperty('forms'), 'Response has a "forms" key')
            t.ok(Array.isArray(response.body.forms), 'Response "forms" key contains an array')
            t.end()
        })
})

test('that a GET forms/status gives data in the correct format', (t) => {
    request(app)
        .get('/form/status')
        .then(response => {
            t.ok(response.body.hasOwnProperty('forms'), 'Response has a "forms" key')
            t.ok(Array.isArray(response.body.forms), 'Response "forms" key contains an array')
            t.end()
        })
})

test('that a GET to forms/status has an issueStatus property for forms with an issueId', t => {
    request(app)
        .get('/form/status')
        .then(response => {
            response.body.forms.forEach(form => {
                if (form.issueId) {
                    t.ok(form.issueStatus, 'Form has an issueStatus with issueId')
                } else {
                    t.notOk(form.issueStatus, 'Form does not have an issueStatus without an issueId')
                }
            })
            t.end()
        })
})

test('that GET form/:id gives data in the correct format when a form is found', t => {
    request(app)
        .post('/form')
        .send({'{"title":"TEST Form"}': ''})
        .then(response => request(app).get(`/form/${response.body.form.id}`))
        .then(response => {
            t.ok(response.body.hasOwnProperty('forms'), 'Response has a "forms" key')
            t.ok(Array.isArray(response.body.forms), 'Response "forms" key contains an array')
            return request(app).delete(`/form/${response.body.forms[0].id}`)
        })
        .then(() => t.end())
        .catch(error => t.notOk(error))
})

test('that GET form/:id gives data in the correct format when no form found', t => {
    request(app)
        .get('/form/notAForm')
        .then(response => {
            t.ok(response.body.hasOwnProperty('forms'), 'Response has a "forms" key')
            t.ok(Array.isArray(response.body.forms), 'Response "forms" key contains an array')
            t.end()
        })
        .catch(error => {
            t.notOK(error)
            return t.end()
        })
})

test('that POST to form/:id/update can update a form', t => {
    request(app)
        .post('/form')
        .send({'{"title":"TEST Form"}': ''})
        .then(response => {
            return request(app)
                .post(`/form/${response.body.form.id}/update`)
                .send({'{"title":"Nutty Squirrel","subtitle":"something else"}': ''})
        })
        .then(response => {
            const actual = response.body.title
            const expected = 'Nutty Squirrel'
            t.equal(actual, expected, 'Update returns the updated data')
            return request(app).delete(`/form/${response.body.id}`)
        })
        .then(() => t.end())
        .catch(error => {
            t.notOk(error)
            t.end()
        })
})

test('that DELETE can delete a form by ID', t => {
    request(app)
        .post('/form')
        .send({'{"title":"Form To Be Deleted"}': ''})
        .then(response => request(app).delete(`/form/${response.body.form.id}`))
        .then(response => {
            const deletedId = response.body.form.id
            return request(app).get(`/form/${deletedId}`)
        })
        .then(response => {
            t.equal(response.body.forms.length, 0, 'Response to GETTING a deleted id has forms of length 0')
            t.end()
        })
        .catch(error => {
            t.notOk(error)
            t.end()
        })
})

test('Close Mongoose Connection', t => {
    t.pass('Closing Mongoose Connection...')
    mongoose.connection.close()
    t.end()
})
