module.exports = (form) => {
    return {
        "fields": {
            "project": {
                "id": "14500"
            },
            "summary": form.title || 'No Title',
            "customfield_13503": form.title, // Form Title
            "customfield_13504": form.subtitle, // Form Subtitle
            "customfield_13501": form.author, // Form Author
            "customfield_13500": JSON.stringify(form), // Form JSON String Field
            "issuetype": {
                "id": "10900"
            },
            "assignee": {
                "name": "lowem"
            },
            "reporter": {
                "name": "lowem"
            },
            "description": `Issue created from form builder http://52.65.191.56/builder/form/${form._id}`,
            "components": [
                {
                    "id": "10000"
                }
            ]
        }
    }
}
