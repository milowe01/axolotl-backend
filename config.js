module.exports = {
    JIRA_API_ENDPOINT: 'https://moe-webservices.atlassian.net/rest/api/latest',
    MONGODB: process.env.NODE_ENV === 'production' ? '13.54.172.191:5000' : '127.0.0.1:27017',
}
